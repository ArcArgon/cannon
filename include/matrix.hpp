#ifndef MATRIX_HPP_L5TG93R6
#define MATRIX_HPP_L5TG93R6

class matrix
{
public:
    // Constructors
    matrix () ;
    matrix (int r, int c) ;
    matrix (int r, int c, int init) ;

    // Destructors
    //virtual ~matrix ();
    ~matrix() ;

    // Accessor functions
    const int getRows () const ;
    const int getCols () const ;
    const float elemAt (int i) const ;

    // Get reference to first element of matrix
    float& front() ;
    const float& front() const ;


private:
    /* data */
    std::vector<float> elems ;
    int rows ;
    int cols ;
} ;

#endif /* end of include guard: MATRIX_HPP_L5TG93R6 */
