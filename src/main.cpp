#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include <mpi.h>
#include <boost/mpi/datatype.hpp>

#include "../include/matrix.hpp"
#include "../include/utils.hpp"

int main(int argc, char *argv[])
{
    MPI::Init(argc, argv);

    srand48(64545152641);

    int rank, size;
    size = MPI::COMM_WORLD.Get_size();
    rank = MPI::COMM_WORLD.Get_rank();

    int rows, cols;
    rows = cols = 4;

    matrix mat(rows, cols);

    int blkrows, blkcols;
    blkrows = blkcols = sqrt(size);

    int nrows, ncols;
    nrows = rows / blkrows;
    ncols = cols / blkcols;

    // reduced size matrix to store block of matrix
    matrix block(nrows, ncols, 0);

    // Creating a data type for blocks
    MPI_Datatype blocktype, blocktype2;
    MPI_Type_vector(nrows, ncols, cols, MPI_FLOAT, &blocktype2);
    MPI_Type_create_resized(blocktype2, 0, sizeof(float), &blocktype);
    MPI_Type_commit(&blocktype);

    // Calculate displacements and counts for scatter
    int disps[nrows*ncols];
    int counts[nrows*ncols];

    for (int i = 0; i < nrows; ++i) {
        for (int j = 0; j < ncols; ++j) {
            disps[i*ncols+j] = i*rows*nrows + j*ncols;
            counts[i*ncols+j] = 1;
        }
    }

    MPI::COMM_WORLD.Scatterv(&mat.front(), counts, disps, blocktype, &block.front(), nrows*ncols, MPI_FLOAT, 0);

    for (int i = 0; i < size; ++i) {
        MPI::COMM_WORLD.Barrier();
        if(i == rank){
            if(rank == 0){
                std::cout << mat << std::endl;
            }
            std::cout << block << std::endl;
        }
    }

    MPI::Finalize();
    return 0;
}
