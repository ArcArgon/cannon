#include <iostream>
#include <vector>

#include "../include/matrix.hpp"

matrix::matrix (int r, int c) : rows{r}, cols{c}, elems(r*c) {
    for (auto& i : elems) {
        i = drand48();
    }
}

matrix::matrix (int r, int c, int init) : rows{r}, cols{c}, elems(r*c, init) {}

matrix::~matrix () {}

const int matrix::getRows () const {
    return this->rows;
}

const int matrix::getCols () const {
    return this->cols;
}

const float matrix::elemAt (int i) const {
    return this->elems[i];
}

float& matrix::front () {
    return this->elems.front();
}

const float& matrix::front () const {
    return this->elems.front();
}

