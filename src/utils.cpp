#include <iostream>
#include <vector>

#include "../include/matrix.hpp"

std::ostream& operator<<(std::ostream& strm, const matrix& mat){
    int rows = mat.getRows();
    int cols = mat.getCols();

    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            strm << mat.elemAt(i*cols+j) << " ";
        }
        strm << std::endl;
    }
    return strm;
}
